from setuptools import setup

setup(
    name="DLKit-lite",
    version="0.1.0",
    description="Digital Learning ToolKit based off of the OSIDS",
    author="Jeff Merriman",
    author_email="birdland@mit.edu",
    url="https://mc3.mit.edu",
    license="MIT",
    install_requires=[
        "sympy",
        "pymongo"
    ]
)